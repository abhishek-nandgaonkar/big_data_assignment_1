Homework 1

•Read all papers on blackboard & abridgeone of the following papers: 
–What’s inside the Cloud 
–Above the Clouds -Berkeley View of Cloud Computing 
–Anatomy of a Large-Scale HypertextualWeb Search Engine 
–The Google File System 
–MapReduce-Simplified Data Processing on Large Clusters 
–The Origin of the VM370 Time-sharing System 
–VM and the VM Community -Past, Present, and Future 
–Pick one of them and write a short paper on it, max 3 pages

•Email to instructor (NU7250@gmail.com) 
–Write a 5-minute presentation•Plan to deliver it next class

•Textbook reading assignments (optional): 
–Chapters 3, 4. 9, and 10

Homework 2

•Set up two Hadoop infrastructures
–Linux people: Pure Hadoop or Cloudera–Windows people: Cygwin + Hadoop
–Mac People: Pure Hadoop–Follow instructions in Installing Hadoopdocument on Blackboard

•Set up Hadoop 0.20.2 andlatest Hadoop

Homework 3

•Install your personal Hypervisor & 1 VM:
–Oracle VirtualBoxor VMWare Workstation
•For Windows hosts•For Linux hosts•For OSX hosts

•Email a picture of your Virtual Machine desktop to NU7250@gmail.com

Homework 4

•Generate all prime numbers:
–Write a prime number generating coroutine
–Solve https://projecteuler.net/problem=7
–What is the 10,001stPrimonaccinumber?

•Primonacci= Fibonacci sequence of primes instead of naturals

•You can do it in Visual Studio (C#) or Eclipse (Java)
–You will need Java 8 JDK for lambdas for Eclipse