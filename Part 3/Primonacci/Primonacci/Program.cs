﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Primonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("===============================================================");
            Console.WriteLine("Waiting to print the first ten Primonacci numbers on your command");
            Console.WriteLine("===============================================================");
            Console.WriteLine("");
            Console.WriteLine("Please hit Enter to confirm");
            Console.WriteLine("===============================================================");
            Console.ReadLine();
            var fibonacci = Unfold2(2, 3, (a, b) => a + b);
            foreach (var x in fibonacci.Take(10))
            {
                Console.WriteLine(x);
            }
            Console.WriteLine("===============================================================");
            Console.WriteLine("Thank you for using the program. Hit Enter to Exit");
            Console.WriteLine("===============================================================");
            Console.ReadLine();

        }

        private static IEnumerable<Decimal> Unfold2<T>(T seed, T seed2, Func<T, T, T> accumulator)
        {
            Boolean flag = false;
            var nextValue = seed;
            Decimal counter = 0;
            List<Decimal> primonacci = new List<Decimal>();
            Decimal DecimalNextValue = Decimal.Parse(seed.ToString());
            if(isPrime(DecimalNextValue))
            {   var a = seed; 
                primonacci.Add(Decimal.Parse(a.ToString()));
                yield return Decimal.Parse(seed.ToString());
                var b = seed2; 
                yield return Decimal.Parse(seed2.ToString());
                primonacci.Add(Decimal.Parse(b.ToString()));
                T c;

                while (true)
                {
                    if (flag)
                    {
                        yield return Decimal.Parse(b.ToString()); 
                        counter++;
                        flag = false;
                    }
                    c = b; 
                    if (isPrime(Decimal.Parse(accumulator(a,b).ToString()))) 
                    {
                        b = accumulator(a, b); 
                        a = c; 
                        flag = true;
                    }
                    else
                    {
                        b = accumulator(a, b); 
                        a = c; 
                    }
                    
                }
            }

            
        }
        static Boolean isPrime(Decimal n)
        {
            if (n == 2) return true;
            if (n % 2 == 0) return false;
            for (Decimal i = 3; i * i <= n; i += 2)
            {
                if (n % i == 0)
                    return false;
            }
            return true;
        }

    }
}




