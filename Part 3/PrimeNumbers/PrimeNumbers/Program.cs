﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumbers
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("===============================================================");
            Console.WriteLine("Waiting to print the first ten prime numbers on your command");
            Console.WriteLine("===============================================================");
            Console.WriteLine("");
            Console.WriteLine("Please hit Enter to confirm");
            Console.WriteLine("===============================================================");
            Console.ReadLine();
            var primeNumbers = Unfold(1, i => i + 1);
            foreach (var x in primeNumbers.Take(10))
            {
                Console.WriteLine(x);
            }
           
            Console.WriteLine("===============================================================");
            Console.WriteLine("Thank you for using the program. Hit Enter to Exit");
            Console.WriteLine("===============================================================");
            Console.ReadLine();
        }
        static Boolean isPrime(int n)
        {
            if (n == 2) return true;
            if (n % 2 == 0) return false;
            for (int i = 3; i * i <= n; i += 2)
            {
                if (n % i == 0)
                    return false;
            }
            return true;
        }

        private static IEnumerable<T> Unfold<T>(T seed, Func<T, T> accumulator)
        {
            var nextValue = seed;
            int intNextValue = int.Parse(seed.ToString());
            
            while (true)
            {
                if ((isPrime(intNextValue)))
                {
                    yield return nextValue;
                    nextValue = accumulator(nextValue);
                    intNextValue = int.Parse(nextValue.ToString());
                }
                else
                {
                    nextValue = accumulator(nextValue);
                    intNextValue = int.Parse(nextValue.ToString());
                }
            }
        }

    }
}
