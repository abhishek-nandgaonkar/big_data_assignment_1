﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrimeNumber10001
{
        class Program
        {
            static void Main(string[] args)
            {
                Console.WriteLine("===============================================================");
                Console.WriteLine("Waiting to print the 100001st prime number on your command");
                Console.WriteLine("===============================================================");
                Console.WriteLine("");
                Console.WriteLine("Please hit Enter to confirm");
                Console.WriteLine("===============================================================");
                Console.ReadLine();
                var primeNumbers = Unfold(1, i => i + 5000);
                foreach (var x in primeNumbers.Take(1))
                {
                    Console.WriteLine(x);
                }

                Console.WriteLine("===============================================================");
                Console.WriteLine("Thank you for using the program. Hit Enter to Exit");
                Console.WriteLine("===============================================================");
                Console.ReadLine();
            }
            static Boolean isPrime(int n)
            {
                if (n == 2) return true;
                if (n % 2 == 0) return false;
                for (int i = 3; i * i <= n; i += 2)
                {
                    if (n % i == 0)
                        return false;
                }
                return true;
            }

            private static IEnumerable<int> Unfold<T>(T seed, Func<T, T> accumulator)
            {
                var nextValue = seed;
                int intNextValue = int.Parse(seed.ToString());

                {
                int n = find1002Prime();
                yield return n;
                }
            }

        private static int find1002Prime()
        {
            List<int> primes = new List<int>();
            int max = 5000;
            int counting = 1;
        
            for (int i = 2; i <= max; i++)
            {
                Boolean flag = false;
                if (i == max && counting < 10002)
                {
                    max += 1000;
                }
                foreach (var p in primes)
                {
                    if (i % p == 0)
                    {
                        flag = true;
                        break;
                    }
                }

                if (!flag)
                {
                    primes.Add(i);
                    counting++;

                    if (counting == 10002)
                        return primes.ElementAt(primes.Count - 1);
                }

             }
            return primes.ElementAt(primes.Count - 1);
        }

        }
    }
